%ifarch aarch64
%global disable_bcc 1
%global _with_bcc --with-pmdabcc=no
%else
%global disable_bcc 0
%global _with_bcc --with-pmdabcc=yes
%endif

%global __python3 python

Name:             pcp
Version:          4.1.3
Summary:          System-level performance monitoring and performance management
Release:          14
License:          GPLv2+ and LGPLv2.1+ and CC-BY
URL:              https://pcp.io
Source0:          https://dl.bintray.com/pcp/source/pcp-4.1.3.tar.gz
Source1:          https://github.com/performancecopilot/pcp-webapp-vector/archive/1.3.1-1/pcp-webapp-vector-1.3.1-1.tar.gz
Source2:          https://github.com/performancecopilot/pcp-webapp-grafana/archive/1.9.1-2/pcp-webapp-grafana-1.9.1-2.tar.gz
Source3:          https://github.com/performancecopilot/pcp-webapp-graphite/archive/0.9.10/pcp-webapp-graphite-0.9.10.tar.gz
Source4:          https://github.com/performancecopilot/pcp-webapp-blinkenlights/archive/1.0.1/pcp-webapp-blinkenlights-1.0.1.tar.gz
Patch0000:        0000-Fix-some-options-and-syntax-errors.patch
Patch0001:        0001-Fix-collectl2pcp-option.patch
Patch6000:        CVE-2019-3695-CVE-2019-3696.patch
Patch9000:        9000-fix-new-function-not-found-when-Install.patch
Patch9001:        0002-fix-some-pcp-pidstat-bugs.patch
Patch9002:        0003-fix-pcp-pidstat-bug.patch
Patch9003:        0004-modify-python2-to-python3.patch

BuildRequires:    gcc gcc-c++ procps autoconf bison flex nss-devel rpm-devel avahi-devel xz-devel zlib-devel
BuildRequires:    python3-devel ncurses-devel readline-devel cyrus-sasl-devel papi-devel libpfm-devel >= 4
BuildRequires:    libmicrohttpd-devel cairo-devel systemtap-sdt-devel boost-devel perl-generators perl-devel perl(strict)
BuildRequires:    perl(ExtUtils::MakeMaker) perl(LWP::UserAgent) perl(JSON) perl(LWP::UserAgent) perl(Time::HiRes) perl(Digest::MD5)
BuildRequires:    man systemd-devel desktop-file-utils qt5-qtbase-devel qt5-qtsvg-devel
Requires:         bash bc bzip2 gawk gcc sed grep findutils which pcp-selinux = %{version}-%{release}
Requires:         pcp-conf = %{version}-%{release}
Provides:         pcp-pmda-kvm pcp-libs = %{version}-%{release}
Obsoletes:        pcp-pmda-kvm pcp-libs < %{version}-%{release}
Obsoletes:        pcp-pmda-nvidia pcp-compat
Conflicts:        librapi
%description
PCP provides a range of services that may be used to monitor and manage system performance.
These services are distributed and scalable to accommodate the most complex system configurations and performance problems.

%package conf
Summary:          PCP run-time configuration
License:          LGPLv2.1+
Conflicts:        pcp-libs < 3.9
%description conf
PCP run-time configuration files

%package devel
Summary:          PCP development tools
License:          GPLv2+ and LGPLv2.1+
Requires:         pcp = %{version}-%{release}
Provides:         pcp-libs-devel = %{version}-%{release} pcp-testsuite = %{version}-%{release}
Obsoletes:        pcp-libs-devel < %{version}-%{release} pcp-testsuite < %{version}-%{release} pcp-gui-testsuite
%description devel
PCP tools for development that contains headers, test and libs abort PCP.

%package help
Summary:          Documents for %{name}
Buildarch:        noarch
Requires:         man info
Provides:         pcp-doc = %{version}-%{release}
Obsoletes:        pcp-doc < %{version}-%{release}
%description help
Man pages and other related documents for %{name}.


%package manager
Summary:          PCP manager daemon
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description manager
PCP optional daemon manages a set of pmlogger and pmie daemons.

%package webapi
Summary:          Web API service for PCP
License:          GPLv2+
Requires:         pcp = %{version}-%{release} liberation-sans-fonts
%description webapi
This package provides a daemon (pmwebd) that uses the HTTP (PMWEBAPI) protocol to bind a larger subset of
the Performance Co-Pilot Client API (PMAPI) to a RESTful web application.

%package webjs
Summary:          PCP web applications
License:          ASL 2.0 and MIT and CC-BY and GPLv3
Conflicts:        pcp-webjs < 3.11.9
BuildArch:        noarch
Requires:         pcp-webapp-vector pcp-webapp-blinkenlights pcp-webapp-graphite pcp-webapp-grafana

%description webjs
Javascript web application content for the PCP web service.

%package webapp-vector
Summary:          PCP's Vector Web Application
License:          ASL 2.0
BuildArch:        noarch
URL:              https://github.com/Netflix/vector
%description webapp-vector
PCP's Vector Web Application

%package webapp-grafana
Summary:          Grafana web application for PCP
License:          ASL 2.0
URL:              https://grafana.org
Conflicts:        pcp-webjs < 3.10.4
BuildArch:        noarch
%description webapp-grafana
Grafana is an open source, feature-rich indicator dashboard and graphical editor. The package
provides a Grafana, which uses Performance Co-Pilot as a data repository. No other Grafana backends
are used.Grafana can render time series dashboards in the browser via float.js (interactive, slower,
for powerful browsers), or via png on the server (less interactive, faster).

%package webapp-graphite
Summary:          Graphite web application for PCP
License:          ASL 2.0 and GPLv3
URL:              http://graphite.readthedocs.org
Conflicts:        pcp-webjs < 3.10.4
BuildArch:        noarch

%description webapp-graphite
Graphite is a highly scalable real-time graphics system. The package provides a graphite version
that uses PCP as its data repository and presents it in the Graphites Web interface. Carbon and
whispering subsystems that do not include or use graphite.

%package webapp-blinkenlights
License:          ASL 2.0
Summary:          This is the blinking light web application for PCP
BuildArch:        noarch
%description webapp-blinkenlights
Demonstrate a web application that displays traffic lights that can change color based on regular
evaluation of performance indicator expressions.

%package -n perl-PCP-PMDA
Summary:          PCP Perl bindings and documentation
License:          GPLv2+
Requires:         pcp = %{version}-%{release} perl-interpreter
%description -n perl-PCP-PMDA
The PCP::PMDA Perl module contains the language bindings that can be used to build
Performance Metric Domain Agents (PMDA) using Perl.
Each PMDA exports performance data for a specific domain, such as the operating system
kernel, databases，applications，Cisco routers etc.

%package -n perl-PCP-MMV
Summary:          PCP Perl bindings for PCP Memory Mapped Values
License:          GPLv2+
Requires:         pcp = %{version}-%{release} perl-interpreter
%description -n perl-PCP-MMV
The PCP :: MMV module contains Perl language bindings that can be used to build scripts that are
detected using the PCP memory mapped value (MMV) mechanism.This mechanism can export arbitrary
values from the detection script to the PCP infrastructure, making it easy to monitor and analyze
using pmchart, pmie, pmlogger and other PCP tools.

%package -n perl-PCP-LogImport
Summary:          PCP Perl bindings for importing external archive data into PCP archives
License:          GPLv2+
Requires:         pcp = %{version}-%{release} perl-interpreter
%description -n perl-PCP-LogImport
The PCP::LogImport module contains Perl language bindings, which can import data from a variety of
third-party formats into PCP archives, making it easy to replay them using standard PCP monitoring
tools.

%package -n perl-PCP-LogSummary
Summary:          PCP Perl bindings for post-processing output of pmlogsummary
License:          GPLv2+
Requires:         pcp = %{version}-%{release} perl-interpreter

%description -n perl-PCP-LogSummary
The PCP :: LogSummary module provides a Perl module that can be used to generate statistical
summary data using the PCP pmlogsummary utility.This utility generates various averages, minimums,
maximums and other calculations based on performance data stored in the PCP archive.The Perl
interface is ideal for exporting data to third-party tools such as spreadsheets.

%package import-sar2pcp
Summary:          PCP tools that can importing sar data into PCP archive logs
License:          LGPLv2+
Requires:         pcp = %{version}-%{release} perl-PCP-LogImport = %{version}-%{release} perl(XML::TokeParser)
%description import-sar2pcp
PCP front-end tool that can import sar data into standard PCP archive logs for replay with
any PCP monitoring tool.

%package import-iostat2pcp
Summary:          PCP tools that can import iostat data into PCP archive logs
License:          LGPLv2+
Requires:         pcp = %{version}-%{release} perl-PCP-LogImport = %{version}-%{release}
%description import-iostat2pcp
PCP front-end tool, which can import sar data into standard PCP archive logs, and can be replayed
with any PCP monitoring tool.

%package import-mrtg2pcp
Summary:          PCP tool that can import MTRG data into PCP archive logs
License:          LGPLv2+
Requires:         pcp = %{version}-%{release} perl-PCP-LogImport = %{version}-%{release}
%description import-mrtg2pcp
PCP front-end tool that can import MTRG data into standard PCP archive logs, and can be replay with
any PCP monitoring tool.

%package import-ganglia2pcp
Summary:          Import ganglion data into PCP archive log using PCP tool
License:          LGPLv2+
Requires:         pcp = %{version}-%{release} perl-PCP-LogImport = %{version}-%{release}

%description import-ganglia2pcp
Use PCP front-end tools to import ganglion data into standard PCP archive logs, and can be replay with
any PCP monitoring tool.

%package import-collectl2pcp
Summary:          Use PCP tools to import collected log files into PCP archive logs
License:          LGPLv2+
Requires:         pcp = %{version}-%{release}

%description import-collectl2pcp
PCP front-end tools that can importing collectl data into standard PCP archive logs, and can be replay
with any PCP monitoring tool.

%package export-zabbix-agent
Summary:          Module that can export PCP indicators to Zabbix agent
License:          GPLv2+
Requires:         pcp = %{version}-%{release}

%description export-zabbix-agent
Module that can export PCP indicators from PCP to Zabbix via the Zabbix agent
- see zbxpcp(3) for further details.

%package export-pcp2graphite
Summary:          PCP tools for exporting PCP metrics to Graphite
License:          GPLv2+
Requires:         pcp >= %{version}-%{release} python3-pcp = %{version}-%{release}
%description export-pcp2graphite
PCP front-end tools for exporting metric values to graphite (http://graphite.readthedocs.org).

%package export-pcp2influxdb
Summary:          PCP tools for exporting PCP metrics to InfluxDB
License:          GPLv2+
Requires:         pcp >= %{version}-%{release} python3-pcp = %{version}-%{release} python3-requests
%description export-pcp2influxdb
PCP front-end tools for exporting metric values
to InfluxDB (https://influxdata.com/time-series-platform/influxdb).

%package export-pcp2json
Summary:          Use PCP tools to export PCP indicators in JSON format
License:          GPLv2+
Requires:         pcp >= %{version}-%{release} python3-pcp = %{version}-%{release}

%description export-pcp2json
PCP tools for exporting PCP metrics in JSON format.

%package export-pcp2spark
Summary:          Export PCP metrics to Apache Spark using PCP tools
License:          GPLv2+
Requires:         pcp >= %{version}-%{release} python3-pcp = %{version}-%{release}

%description export-pcp2spark
Use the PCP front-end tool to export JSON-formatted indicator values to Apache Spark.
See https://spark.apache.org/ for further details on Apache Spark.

%package export-pcp2xml
Summary:          PCP front-end tools for exporting PCP metrics in XML format
License:          GPLv2+
Requires:         pcp >= %{version}-%{release} python3-pcp = %{version}-%{release}

%description export-pcp2xml
Use PCP front-end tools to export metric values in XML format.

%package export-pcp2zabbix
Summary:          PCP tool that can export PCP indicators to Zabbix
License:          GPLv2+
Requires:         python3-pcp = %{version}-%{release} pcp >= %{version}-%{release}

%description export-pcp2zabbix
PCP front-end tools for exporting metric values to the Zabbix
(https://www.zabbix.org/) monitoring software.

%package pmda-papi
Summary:          PCP metrics for hardware counters and Performance API
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
BuildRequires:    papi-devel

%description pmda-papi
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering hardware counters statistics through PAPI (Performance API).

%package pmda-perfevent
Summary:          Hardware counters PCP metrics
License:          GPLv2+
Requires:         pcp = %{version}-%{release} libpfm >= 4
BuildRequires:    libpfm-devel >= 4
%description pmda-perfevent
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering hardware counters statistics through libpfm.

%package pmda-infiniband
Summary:          PCP metrics for Infiniband HCAs and switches
License:          GPLv2+
Requires:         pcp = %{version}-%{release} rdma-core
BuildRequires:    rdma-core-devel
%description pmda-infiniband
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering Infiniband statistics.  By default, it monitors the local HCAs
but can also be configured to monitor remote GUIDs such as IB switches.

%package pmda-activemq
Summary:          ActiveMQ PCP metrics
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} perl(LWP::UserAgent)
%description pmda-activemq
This package provides the PCP Performance Metrics Domain Agent-PMDA which is used for
gathering metrics about the ActiveMQ message broker.

%package pmda-bind2
Summary:          PCP metrics for BIND servers
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} perl(LWP::UserAgent) perl(XML::LibXML) perl(File::Slurp)
%description pmda-bind2
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics from BIND (Berkeley Internet Name Domain).

%package pmda-redis
Summary:          Redis PCP metrics
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-redis
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics from Redis servers (redis.io).

%package pmda-bonding
Summary:          PCP metrics for Bonded network interfaces
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-bonding
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about bonded network interfaces.

%package pmda-dbping
Summary:          PCP metrics for Database response times and Availablility
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-dbping
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the Database response times and Availablility.

%package pmda-ds389
Summary:          PCP metrics for 389 Directory Servers
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-ds389
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about a 389 Directory Server.

%package pmda-ds389log
Summary:          PCP metrics for 389 Directory Server Loggers
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} perl-Date-Manip
%description pmda-ds389log
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics from a 389 Directory Server log.

%package pmda-elasticsearch
Summary:          PCP metrics for Elasticsearch
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} perl(LWP::UserAgent)
BuildRequires:    perl(LWP::UserAgent)
%description pmda-elasticsearch
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about Elasticsearch.

%package pmda-gpfs
Summary:          PCP metrics for GPFS Filesystem
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-gpfs
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the GPFS filesystem.

%package pmda-gpsd
Summary:          PCP metrics for a GPS Daemon
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-gpsd
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about a GPS Daemon.

%package pmda-docker
Summary:          PCP metrics from the Docker daemon
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-docker
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics using the Docker daemon REST API.

%package pmda-lustre
Summary:          PCP metrics for the Lustre Filesytem
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-lustre
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the Lustre Filesystem.

%package pmda-lustrecomm
Summary:          PCP metrics for the Lustre Filesytem Comms
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-lustrecomm
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the Lustre Filesystem Comms.

%package pmda-memcache
Summary:          PCP metrics for Memcached
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-memcache
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about Memcached.

%package pmda-mysql
Summary:          PCP metrics for MySQL
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} perl(DBI) perl(DBD::mysql)
BuildRequires:    perl(DBI) perl(DBD::mysql)
%description pmda-mysql
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the MySQL database.

%package pmda-named
Summary:          PCP metrics for Named
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-named
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the Named nameserver.

%package pmda-netfilter
Summary:          PCP metrics for Netfilter framework
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-netfilter
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the Netfilter packet filtering framework.

%package pmda-news
Summary:          PCP metrics for Usenet News
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-news
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about Usenet News.

%package pmda-nginx
Summary:          PCP metrics for the Nginx Webserver
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} perl(LWP::UserAgent)
BuildRequires:    perl(LWP::UserAgent)
%description pmda-nginx
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the Nginx Webserver.

%package pmda-nfsclient
Summary:          PCP metrics for NFS Clients
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-nfsclient
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics for NFS Clients.

%package pmda-oracle
Summary:          PCP metrics for the Oracle database
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} perl(DBI)
BuildRequires:    perl(DBI)
%description pmda-oracle
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the Oracle database.

%package pmda-pdns
Summary:          PCP metrics for PowerDNS
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-pdns
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the PowerDNS.

%package pmda-postfix
Summary:          PCP metrics for the Postfix (MTA)
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release} postfix-perl-scripts
BuildRequires:    postfix-perl-scripts
%description pmda-postfix
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the Postfix (MTA).

%package pmda-postgresql
Summary:          PCP metrics for PostgreSQL
License:          GPLv2+
Requires:         python3-pcp python3-psycopg2
BuildRequires:    python3-psycopg2
%description pmda-postgresql
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the PostgreSQL database.

%package pmda-rsyslog
Summary:          PCP metrics for Rsyslog
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-rsyslog
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about Rsyslog.

%package pmda-samba
Summary:          PCP metrics for Samba
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-samba
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about Samba.

%package pmda-slurm
Summary:          PCP metrics for the SLURM Workload Manager
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-slurm
This package collects metrics from the SLURM Workload Manager.

%package pmda-snmp
Summary:          PCP Simple Network Management Protocol metrics
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-snmp
This package collects metrics about SNMP.

%package pmda-vmware
Summary:          VMware PCP metrics
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-vmware
This package collects metrics for VMware.

%package pmda-zimbra
Summary:          Zimbra PCP metrics
License:          GPLv2+
Requires:         perl-PCP-PMDA = %{version}-%{release}
%description pmda-zimbra
This package collects metrics about Zimbra.

%package pmda-dm
Summary:          PCP metrics for the Device Mapper Cache and Thin Client
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-dm
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the Device Mapper Cache and Thin Client.

%if !%{disable_bcc}
%package pmda-bcc
Summary:          Performance Co-Pilot (PCP) metrics from eBPF/BCC modules
License:          ASL 2.0 and GPLv2+
Requires:         python3-bcc python3-pcp
%description pmda-bcc
This package collects metrics about eBPF/BCC Python modules.
%endif

%package pmda-gluster
Summary:          PCP metrics for the Gluster filesystem
License:          GPLv2+
Requires:         python3-pcp
%description pmda-gluster
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the gluster filesystem.

%package pmda-zswap
Summary:          compressed swap PCP metrics
License:          GPLv2+
Requires:         python3-pcp
%description pmda-zswap
This package collects metrics about compressed swap.

%package pmda-unbound
Summary:          Unbound DNS Resolver PCP metrics
License:          GPLv2+
Requires:         python3-pcp
%description pmda-unbound
This package collects metrics about the Unbound DNS Resolver.

%package pmda-mic
Summary:          PCP metrics for Intel MIC cards
License:          GPLv2+
Requires:         python3-pcp
%description pmda-mic
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about Intel MIC cards.

%package pmda-haproxy
Summary:          PCP metrics for HAProxy
License:          GPLv2+
Requires:         python3-pcp
%description pmda-haproxy
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
extracting performance metrics from HAProxy over the HAProxy stats socket.

%package pmda-libvirt
Summary:          PCP metrics for virtual machines
License:          GPLv2+
Requires:         python3-pcp libvirt-python3 python3-lxml
BuildRequires:    libvirt-python3 python3-lxml
%description pmda-libvirt
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
extracting virtualisation statistics from libvirt about behaviour of guest
and hypervisor machines.

%package pmda-lio
Summary:          PCP metrics for the LIO subsystem
License:          GPLv2+
Requires:         python3-pcp python3-rtslib
BuildRequires:    python3-rtslib
%description pmda-lio
This package provides a PMDA to gather performance metrics from the kernels
iSCSI target interface (LIO). The metrics are stored by LIO within the Linux
kernels configfs filesystem. The PMDA provides per LUN level stats, and a
summary instance per iSCSI target, which aggregates all LUN metrics within the
target.

%package pmda-prometheus
Summary:          PCP metrics from Prometheus endpoints
License:          GPLv2+
Requires:         pcp = %{version}-%{release} python3-pcp python3-requests
BuildRequires:    python3-requests
%description pmda-prometheus
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
extracting statistics from programs instrumented as Prometheus endpoints.

%package pmda-json
Summary:          PCP metrics for JSON data
License:          GPLv2+
Requires:         python3-pcp python3-jsonpointer python3-six
BuildRequires:    python3-jsonpointer python3-six
%description pmda-json
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics output in JSON.

%package pmda-apache
Summary:          PCP metrics for the Apache webserver
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-apache
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the Apache webserver.

%package pmda-bash
Summary:          PCP metrics for the Bash shell
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-bash
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the Bash shell.

%package pmda-cifs
Summary:          PCP metrics for the CIFS protocol
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-cifs
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the Common Internet Filesytem.

%package pmda-cisco
Summary:          PCP metrics for Cisco routers
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-cisco
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about Cisco routers.

%package pmda-gfs2
Summary:          PCP metrics for the GFS2 filesystem
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-gfs2
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the Global Filesystem v2.

%package pmda-lmsensors
Summary:          PCP metrics for hardware sensors
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-lmsensors
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the Linux hardware monitoring sensors.

%package pmda-logger
Summary:          PCP metrics from arbitrary log files
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-logger
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics from a specified set of log files (or pipes).  The PMDA
supports both sampled and event-style metrics.

%package pmda-mailq
Summary:          PCP metrics for the sendmail queue
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-mailq
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about email queues managed by sendmail.

%package pmda-mounts
Summary:          PCP metrics for filesystem mounts
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-mounts
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about filesystem mounts.

%package pmda-nvidia-gpu
Summary:          PCP metrics for the Nvidia GPU
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-nvidia-gpu
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about Nvidia GPUs.

%package pmda-roomtemp
Summary:          PCP metrics for the room temperature
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-roomtemp
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the room temperature.

%package pmda-rpm
Summary:          PCP metrics for the RPM package manager
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-rpm
This package provides the PCP Performance Metrics Domain Agent -- PMDA which is used for
gathering metrics about the installed RPM packages.

%package pmda-sendmail
Summary:          PCP Sendmail metrics
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-sendmail
This package collects metrics about Sendmail traffic.

%package pmda-shping
Summary:          PCP shell command responses metrics
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-shping
This package collects metrics about response time of shell commands.

%package pmda-smart
Summary:          PCP metrics for S.M.A.R.T values
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-smart
This package collects metrics of disk S.M.A.R.T values.

%package pmda-summary
Summary:          PCP summary metrics from pmie
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-summary
This package collects metrics about other installed pmdas.

%package pmda-systemd
Summary:          PCP metrics from the Systemd journal
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-systemd
This package collects metrics from the Systemd journal.

%package pmda-trace
Summary:          application tracing PCP metrics
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-trace
This package collects metrics about trace performance data in applications.

%package pmda-weblog
Summary:          web server logs PCP metrics
License:          GPLv2+
Requires:         pcp = %{version}-%{release}
%description pmda-weblog
This package collects metrics about web server logs.

%package collector
Summary:          PCP Collection meta Package
License:          GPLv2+
Requires:         pcp-pmda-activemq pcp-pmda-bonding pcp-pmda-dbping pcp-pmda-ds389 pcp-pmda-ds389log
Requires:         pcp-pmda-elasticsearch pcp-pmda-gpfs pcp-pmda-gpsd pcp-pmda-lustre
Requires:         pcp-pmda-memcache pcp-pmda-mysql pcp-pmda-named pcp-pmda-netfilter pcp-pmda-news
Requires:         pcp-pmda-nginx pcp-pmda-nfsclient pcp-pmda-pdns pcp-pmda-postfix pcp-pmda-postgresql pcp-pmda-oracle
Requires:         pcp-pmda-samba pcp-pmda-slurm pcp-pmda-vmware pcp-pmda-zimbra pcp-pmda-dm pcp-pmda-apache
Requires:         pcp-pmda-bash pcp-pmda-cisco pcp-pmda-gfs2 pcp-pmda-lmsensors pcp-pmda-mailq pcp-pmda-mounts
Requires:         pcp-pmda-nvidia-gpu pcp-pmda-roomtemp pcp-pmda-sendmail pcp-pmda-shping pcp-pmda-smart
Requires:         pcp-pmda-lustrecomm pcp-pmda-logger pcp-pmda-docker pcp-pmda-bind2 pcp-pmda-gluster pcp-pmda-zswap
Requires:         pcp-pmda-libvirt pcp-pmda-lio pcp-pmda-prometheus pcp-pmda-haproxy pcp-pmda-snmp pcp-pmda-json
Requires:         pcp-pmda-rpm pcp-pmda-summary pcp-pmda-trace pcp-pmda-weblog pcp-pmda-unbound pcp-pmda-mic
%if !%{disable_bcc}
Requires: pcp-pmda-bcc
%endif
%description collector
This package will install the PCP metric collection dependencies used to collect PCP metrics.

%package monitor
Summary:          PCP Monitoring meta Package
License:          GPLv2+
Requires:         pcp-webapi pcp-system-tools pcp-gui
%description monitor
This meta-package contains the PCP performance monitoring dependencies.  This
includes a large number of packages for analysing PCP metrics in various ways.

%package zeroconf
Summary:          PCP Zeroconf Package
License:          GPLv2+
Requires:         pcp pcp-doc pcp-system-tools pcp-pmda-dm pcp-pmda-nfsclient
%description zeroconf
This package installs configuration files to tweak indexes for PCP metrics.

%package -n python3-pcp
Summary:          PCP Python3 bindings and documentation
License:          GPLv2+
Requires:         pcp = %{version}-%{release} python3
%description -n python3-pcp
This package contains language bindings for the Performance Metric API (PMAPI) monitoring
tool and Performance Metric Domain Agent (PMDA) collector tool written in Python3.

%package system-tools
Summary:          PCP System and Monitoring Tools
License:          GPLv2+
Requires:         python3-pcp = %{version}-%{release} pcp = %{version}-%{release}
Obsoletes:        dstat
Provides:         /usr/bin/dstat
%description system-tools
This package contains some Python system monitoring tools.

%package gui
Summary:          PCP toolkit visual tools
License:          GPLv2+ and LGPLv2+ and LGPLv2+ with exceptions
BuildRequires:    hicolor-icon-theme
Requires:         pcp = %{version}-%{release} liberation-sans-fonts
%description gui
Visualization tools for the PCP toolkit.

%package selinux
Summary:          PCP selinux policy
License:          GPLv2+ and CC-BY
BuildRequires:    selinux-policy-devel selinux-policy-targeted setools-console
Requires:         policycoreutils
%description selinux
This package installs selinux support files for PCP.

%prep
%setup -q -T -D -a 1 -c -n vector
%setup -q -T -D -a 2 -c -n grafana
%setup -q -T -D -a 3 -c -n graphite
%setup -q -T -D -a 4 -c -n blinkenlights
%setup -q
%patch0000 -p1
%patch0001 -p1
%patch6000 -p1
%patch9000 -p1
%patch9001 -p1
%patch9002 -p1
%patch9003 -p1

%build

%configure  --with-docdir=/usr/share/doc/pcp \
--with-dstat-symlink=yes \
--with-infiniband=yes \
--with-papi=yes \
--with-perfevent=yes \
%{?_with_bcc} \
--with-pmdajson=yes \
--with-pmdasnmp=yes \
--with-pmdanutcracker=no \
--with-webapps=yes
sed -i 's/make/make -j1/' src/selinux/GNUmakefile

%make_build default_pcp

%install
export NO_CHOWN=true DIST_ROOT=$RPM_BUILD_ROOT
make install_pcp
PCP_GUI='pmchart|pmconfirm|pmdumptext|pmmessage|pmquery|pmsnap|pmtime'
rm -f $RPM_BUILD_ROOT/usr/lib64/*.a
rm -f $RPM_BUILD_ROOT/usr/bin/sheet2pcp $RPM_BUILD_ROOT/usr/share/man/man1/sheet2pcp.1*
rm -f $RPM_BUILD_ROOT/usr/include/pcp/configsz.h
rm -f $RPM_BUILD_ROOT/usr/include/pcp/platformsz.h
for app in vector grafana graphite blinkenlights; do
    webapp=`find ../$app -mindepth 1 -maxdepth 1`
    cp -r -f $webapp $RPM_BUILD_ROOT/usr/share/pcp/webapps/$app
done
rm -rf $RPM_BUILD_ROOT/usr/share/doc/pcp-gui
desktop-file-validate $RPM_BUILD_ROOT/usr/share/applications/pmchart.desktop
for f in $RPM_BUILD_ROOT/usr/share/pcp/lib/{pcp,pmcd,pmlogger,pmie,pmwebd,pmmgr,pmproxy}; do
    test -f "$f" || continue
    sed -i -e '/^# chkconfig/s/:.*$/: - 95 05/' -e '/^# Default-Start:/s/:.*$/:/' $f
done
if [ "$1" -eq 1 ]
then
PCP_SYSCONFIG_DIR=/etc/sysconfig
sed -i 's/^\#\ PMLOGGER_LOCAL.*/PMLOGGER_LOCAL=1/g' "$RPM_BUILD_ROOT/$PCP_SYSCONFIG_DIR/pmlogger"
sed -i 's/^\#\ PMCD_LOCAL.*/PMCD_LOCAL=1/g' "$RPM_BUILD_ROOT/$PCP_SYSCONFIG_DIR/pmcd"
fi
ls -1 $RPM_BUILD_ROOT/var/lib/pcp/pmdas |\
  grep -E -v '^simple|sample|trivial|txmon' | grep -E -v '^perfevent|perfalloc.1' |\
  grep -E -v '^ib$|^infiniband' | grep -E -v '^activemq' | grep -E -v '^bonding' |\
  grep -E -v '^bind2' | grep -E -v '^dbping' | grep -E -v '^docker' |\
  grep -E -v '^ds389log'| grep -E -v '^ds389' | grep -E -v '^elasticsearch' |\
  grep -E -v '^gpfs' | grep -E -v '^gpsd' | grep -E -v '^lio' |\
  grep -E -v '^lustre' | grep -E -v '^lustrecomm' | grep -E -v '^memcache' |\
  grep -E -v '^mysql' | grep -E -v '^named' | grep -E -v '^netfilter' |\
  grep -E -v '^news' | grep -E -v '^nfsclient' | grep -E -v '^nginx' |\
  grep -E -v '^nutcracker' | grep -E -v '^oracle' | grep -E -v '^papi' |\
  grep -E -v '^pdns' | grep -E -v '^postfix' | grep -E -v '^postgresql' |\
  grep -E -v '^redis' | grep -E -v '^rsyslog' | grep -E -v '^samba' |\
  grep -E -v '^slurm' | grep -E -v '^snmp' | grep -E -v '^vmware' |\
  grep -E -v '^zimbra' | grep -E -v '^dm' | grep -E -v '^apache' |\
  grep -E -v '^bash' | grep -E -v '^cifs' | grep -E -v '^cisco' |\
  grep -E -v '^gfs2' | grep -E -v '^libvirt' | grep -E -v '^lmsensors' |\
  grep -E -v '^logger' | grep -E -v '^mailq' | grep -E -v '^mounts' |\
  grep -E -v '^nvidia' | grep -E -v '^roomtemp' | grep -E -v '^sendmail' |\
  grep -E -v '^shping' | grep -E -v '^smart' | grep -E -v '^summary' |\
  grep -E -v '^trace' | grep -E -v '^weblog' | grep -E -v '^rpm' |\
  grep -E -v '^json' | grep -E -v '^mic' | grep -E -v '^bcc' |\
  grep -E -v '^gluster' | grep -E -v '^zswap' | grep -E -v '^unbound' |\
  grep -E -v '^haproxy' | sed -e 's#^#'/var/lib/pcp/pmdas'\/#' >base_pmdas.list
ls -1 $RPM_BUILD_ROOT/usr/bin |\
  grep -E -v 'pmiostat|pmcollectl|zabbix|zbxpcp|dstat' |\
  grep -E -v 'pmrep|pcp2graphite|pcp2influxdb|pcp2zabbix' |\
  grep -E -v 'pcp2elasticsearch|pcp2json|pcp2xlsx|pcp2xml' |\
  grep -E -v 'pcp2spark' |\
  grep -E -v 'pmdbg|pmclient|pmerr|genpmda' |\
sed -e 's#^#'/usr/bin'\/#' >base_bin.list
ls -1 $RPM_BUILD_ROOT/usr/bin |\
  egrep -e 'pmiostat|pmcollectl|pmrep|dstat' |\
  sed -e 's#^#'/usr/bin'\/#' >pcp-system-tools.list
ls -1 $RPM_BUILD_ROOT/usr/libexec/pcp/bin |\
  egrep -e 'atop|collectl|dmcache|dstat|free|iostat|ipcs|lvmcache|mpstat' \
        -e 'numastat|pidstat|shping|tapestat|uptime|verify' |\
  sed -e 's#^#'/usr/libexec/pcp/bin'\/#' >>pcp-system-tools.list
ls -1 $RPM_BUILD_ROOT/var/lib/pcp/selinux |\
  sed -e 's#^#'/var/lib/pcp/selinux'\/#' > pcp-selinux.list
ls -1 $RPM_BUILD_ROOT/usr/libexec/pcp/bin |\
  grep -E 'selinux-setup' |\
  sed -e 's#^#'/usr/libexec/pcp/bin'\/#' >> pcp-selinux.list
ls -1 $RPM_BUILD_ROOT/usr/libexec/pcp/bin |\
  grep -E -v 'atop|collectl|dmcache|dstat|free|iostat|mpstat|numastat' |\
  grep -E -v 'shping|tapestat|uptime|verify|selinux-setup' |\
  sed -e 's#^#'/usr/libexec/pcp/bin'\/#' >base_exec.list
ls -1 $RPM_BUILD_ROOT/usr/share/doc/pcp-doc |\
  sed -e 's#^#'/usr/share/doc/pcp-doc'\/#' > pcp-doc.list
ls -1 $RPM_BUILD_ROOT/usr/share/man/man1 |\
  sed -e 's#^#'/usr/share/man'\/man1\/#' >>pcp-doc.list
ls -1 $RPM_BUILD_ROOT/usr/share/man/man5 |\
  sed -e 's#^#'/usr/share/man'\/man5\/#' >>pcp-doc.list
ls -1 $RPM_BUILD_ROOT/usr/share/pcp/demos/tutorials |\
  sed -e 's#^#'/usr/share/pcp/demos/tutorials'\/#' >>pcp-doc.list
ls -1 $RPM_BUILD_ROOT/usr/share/pcp-gui/pixmaps |\
  sed -e 's#^#'/usr/share/pcp-gui/pixmaps'\/#' > pcp-gui.list
ls -1 $RPM_BUILD_ROOT/usr/share/icons/hicolor |\
  sed -e 's#^#'/usr/share/icons/hicolor'\/#' >> pcp-gui.list
cat base_bin.list base_exec.list |\
  grep -E "$PCP_GUI" >> pcp-gui.list
ls -1 $RPM_BUILD_ROOT/var/lib/pcp/config/pmlogconf/ |\
    sed -e 's#^#'/var/lib/pcp/config/pmlogconf'\/#' |\
    grep -E -v 'zeroconf' >pcp-logconf.list
cat base_pmdas.list base_bin.list base_exec.list pcp-logconf.list |\
  grep -E -v 'pmdaib|pmmgr|pmweb|pmsnap|2pcp|pmdas/systemd' |\
  grep -E -v "$PCP_GUI|pixmaps|hicolor|pcp-doc|tutorials|selinux" |\
  grep -E -v /etc/pcp | grep -E -v /var/log/pcp > base.list
ls -1 $RPM_BUILD_ROOT/usr/share/man/man3 |\
sed -e 's#^#'/usr/share/man'\/man3\/#' | grep -v '3pm' >>pcp-doc.list
ls -1 $RPM_BUILD_ROOT/usr/share/pcp/demos |\
sed -e 's#^#'/usr/share'\/pcp\/demos\/#' | grep -E -v tutorials >> devel.list
ls -1 $RPM_BUILD_ROOT/usr/bin |\
grep -E 'pmdbg|pmclient|pmerr|genpmda' |\
sed -e 's#^#'/usr/bin'\/#' >>devel.list

%pre devel
test -d /var/lib/pcp/testsuite || mkdir -p -m 755 /var/lib/pcp/testsuite
getent group pcpqa >/dev/null || groupadd -r pcpqa
getent passwd pcpqa >/dev/null || \
  useradd -c "PCP Quality Assurance" -g pcpqa -d /var/lib/pcp/testsuite -M -r -s /bin/bash pcpqa 2>/dev/null
chown -R pcpqa:pcpqa /var/lib/pcp/testsuite 2>/dev/null
exit 0

%post devel
chown -R pcpqa:pcpqa /var/lib/pcp/testsuite 2>/dev/null
exit 0

%pre
getent group pcp >/dev/null || groupadd -r pcp
getent passwd pcp >/dev/null || \
  useradd -c "PCP" -g pcp -d /var/lib/pcp -M -r -s /sbin/nologin pcp
PCP_CONFIG_DIR=/var/lib/pcp/config
PCP_SYSCONF_DIR=/etc/pcp
PCP_LOG_DIR=/var/log/pcp
PCP_ETC_DIR=/etc
for crontab in pmlogger pmie
do
    test -f "$PCP_ETC_DIR/cron.d/$crontab" || continue
    mv -f "$PCP_ETC_DIR/cron.d/$crontab" "$PCP_ETC_DIR/cron.d/pcp-$crontab"
done
save_configs_script()
{
    _new="$1"
    shift
    for _dir
    do
        [ "$_dir" = "$_new" ] && continue
        if [ -d "$_dir" ]
        then
            ( cd "$_dir" ; find . -maxdepth 1 -type f ) | sed -e 's/^\.\///' \
            | while read _file
            do
                [ "$_file" = "control" ] && continue
                _want=true
                if [ -f "$_new/$_file" ]
                then
                    _try=`find "$_dir/$_file" -newer "$_new/$_file" -print`
                    [ -n "$_try" ] || _want=false
                fi
                $_want && echo cp -p "$_dir/$_file" "$_new/$_file"
            done
        fi
    done
}
[ -d "$PCP_LOG_DIR" ] || exit 0
rm -f "$PCP_LOG_DIR/configs.sh"
for daemon in pmie pmlogger
do
    save_configs_script >> "$PCP_LOG_DIR/configs.sh" "$PCP_CONFIG_DIR/$daemon" \
        "$PCP_SYSCONF_DIR/$daemon"
done
for daemon in pmcd pmproxy
do
    save_configs_script >> "$PCP_LOG_DIR/configs.sh" "$PCP_SYSCONF_DIR/$daemon"\
        "$PCP_CONFIG_DIR/$daemon" /etc/$daemon
done
exit 0

%preun webapi
if [ "$1" -eq 0 ]
then
    systemctl --no-reload disable pmwebd.service >/dev/null 2>&1
    systemctl stop pmwebd.service >/dev/null 2>&1
fi

%preun manager
if [ "$1" -eq 0 ]
then
    systemctl --no-reload disable pmmgr.service >/dev/null 2>&1
    systemctl stop pmmgr.service >/dev/null 2>&1
fi

%preun pmda-rpm
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"rpm"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"rpm"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-papi
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"papi"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"papi"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-systemd
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"systemd"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"systemd"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-infiniband
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"infiniband"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"infiniband"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-perfevent
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"perfevent"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"perfevent"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-json
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"json"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"json"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-nginx
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"nginx"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"nginx"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-oracle
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"oracle"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"oracle"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-postgresql
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"postgresql"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"postgresql"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-postfix
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"postfix"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"postfix"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-elasticsearch
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"elasticsearch"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"elasticsearch"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-snmp
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"snmp"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"snmp"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-mysql
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"mysql"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"mysql"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-activemq
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"activemq"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"activemq"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-bind2
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"bind2"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"bind2"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-bonding
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"bonding"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"bonding"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-dbping
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"dbping"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"dbping"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-docker
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"docker"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"docker"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-ds389
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"ds389"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"ds389"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-ds389log
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"ds389log"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"ds389log"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-gpfs
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"gpfs"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"gpfs"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-gpsd
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"gpsd"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"gpsd"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-lio
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"lio"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"lio"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-prometheus
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"prometheus"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"prometheus"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-lustre
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"lustre"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"lustre"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-lustrecomm
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"lustrecomm"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"lustrecomm"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-memcache
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"memcache"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"memcache"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-named
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"named"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"named"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-netfilter
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"netfilter"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"netfilter"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-news
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"news"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"news"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-nfsclient
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"nfsclient"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"nfsclient"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-pdns
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"pdns"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"pdns"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-rsyslog
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"rsyslog"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"rsyslog"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-redis
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"redis"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"redis"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-samba
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"samba"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"samba"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-vmware
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"vmware"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"vmware"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-zimbra
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"zimbra"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"zimbra"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-dm
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"dm"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"dm"/ && ./Remove >/dev/null 2>&1)
    fi
fi
%if !%{disable_bcc}
%preun pmda-bcc
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"bcc"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"bcc"/ && ./Remove >/dev/null 2>&1)
    fi
fi
%endif

%preun pmda-gluster
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"gluster"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"gluster"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-zswap
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"zswap"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"zswap"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-unbound
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"unbound"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"unbound"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-mic
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"mic"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"mic"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-haproxy
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"haproxy"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"haproxy"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-libvirt
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"libvirt"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"libvirt"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-apache
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"apache"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"apache"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-bash
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"bash"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"bash"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-cifs
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"cifs"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"cifs"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-cisco
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"cisco"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"cisco"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-gfs2
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"gfs2"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"gfs2"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-lmsensors
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"lmsensors"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"lmsensors"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-logger
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"logger"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"logger"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-mailq
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"mailq"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"mailq"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-mounts
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"mounts"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"mounts"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-nvidia-gpu
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"nvidia"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"nvidia"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-roomtemp
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"roomtemp"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"roomtemp"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-sendmail
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"sendmail"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"sendmail"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-shping
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"shping"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"shping"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-smart
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"smart"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"smart"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-summary
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"summary"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"summary"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-trace
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"trace"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"trace"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun pmda-weblog
if [ "$1" -eq 0 ]
then
    if [ -f "/etc/pcp/pmcd/pmcd.conf" -a -f "/var/lib/pcp/pmdas/"weblog"/domain.h" ]
    then
    (cd /var/lib/pcp/pmdas/"weblog"/ && ./Remove >/dev/null 2>&1)
    fi
fi

%preun
if [ "$1" -eq 0 ]
then


if [ $1 -eq 0 ] && [ -x /usr/bin/systemctl ] ; then
        /usr/bin/systemctl --no-reload disable --now pmlogger.service || :
fi


if [ $1 -eq 0 ] && [ -x /usr/bin/systemctl ] ; then
        /usr/bin/systemctl --no-reload disable --now pmie.service || :
fi


if [ $1 -eq 0 ] && [ -x /usr/bin/systemctl ] ; then
        /usr/bin/systemctl --no-reload disable --now pmproxy.service || :
fi


if [ $1 -eq 0 ] && [ -x /usr/bin/systemctl ] ; then
        /usr/bin/systemctl --no-reload disable --now pmcd.service || :
fi
systemctl stop pmlogger.service >/dev/null 2>&1
systemctl stop pmie.service >/dev/null 2>&1
systemctl stop pmproxy.service >/dev/null 2>&1
systemctl stop pmcd.service >/dev/null 2>&1
PCP_PMNS_DIR=/var/lib/pcp/pmns
rm -f "$PCP_PMNS_DIR/.NeedRebuild" >/dev/null 2>&1
fi

%post webapi
chown -R pcp:pcp /var/log/pcp/pmwebd 2>/dev/null
systemctl condrestart pmwebd.service >/dev/null 2>&1

%post manager
chown -R pcp:pcp /var/log/pcp/pmmgr 2>/dev/null
systemctl condrestart pmmgr.service >/dev/null 2>&1

%post collector
systemctl restart pmcd >/dev/null 2>&1
systemctl restart pmlogger >/dev/null 2>&1
systemctl enable pmcd >/dev/null 2>&1
systemctl enable pmlogger >/dev/null 2>&1

%post zeroconf
PCP_PMDAS_DIR=/var/lib/pcp/pmdas
PCP_SYSCONFIG_DIR=/etc/sysconfig
for PMDA in dm nfsclient ; do
    touch "$PCP_PMDAS_DIR/$PMDA/.NeedInstall"
done
sed -i 's/^\#\ PMLOGGER_INTERVAL.*/PMLOGGER_INTERVAL=10/g' "$PCP_SYSCONFIG_DIR/pmlogger"
pmieconf -c enable dmthin
systemctl restart pmcd >/dev/null 2>&1
systemctl restart pmlogger >/dev/null 2>&1
systemctl restart pmie >/dev/null 2>&1
systemctl enable pmcd >/dev/null 2>&1
systemctl enable pmlogger >/dev/null 2>&1
systemctl enable pmie >/dev/null 2>&1

%post selinux
if [ "$1" -ge 1 ]
then
    /usr/libexec/pcp/bin/selinux-setup /var/lib/pcp/selinux install "pcpupstream"
elif [ "$1" -eq 0 ]
then
    /usr/libexec/pcp/bin/selinux-setup /var/lib/pcp/selinux remove "pcpupstream"
fi
%triggerin selinux -- docker-selinux
if [ "$1" -ge 1 ]
then
    /usr/libexec/pcp/bin/selinux-setup /var/lib/pcp/selinux install "pcpupstream-docker"
elif [ "$1" -eq 0 ]
then
    /usr/libexec/pcp/bin/selinux-setup /var/lib/pcp/selinux remove "pcpupstream-docker"
fi
%triggerin selinux -- container-selinux
if [ "$1" -ge 1 ]
then
    /usr/libexec/pcp/bin/selinux-setup /var/lib/pcp/selinux install "pcpupstream-container"
elif [ "$1" -eq 0 ]
then
    /usr/libexec/pcp/bin/selinux-setup /var/lib/pcp/selinux remove "pcpupstream-container"
fi

%if !%{disable_bcc}
%post pmda-bcc
if grep -q ^"bcc" "/etc/pcp/pmcd/pmcd.conf" 2>/dev/null
then
    touch /var/lib/pcp/pmdas/"bcc"/.NeedInstall
fi
%endif

%post pmda-prometheus
if grep -q ^"prometheus" "/etc/pcp/pmcd/pmcd.conf" 2>/dev/null
then
    touch /var/lib/pcp/pmdas/"prometheus"/.NeedInstall
fi

%post
PCP_LOG_DIR=/var/log/pcp
PCP_PMNS_DIR=/var/lib/pcp/pmns
test -s "$PCP_LOG_DIR/configs.sh" && source "$PCP_LOG_DIR/configs.sh"
rm -f $PCP_LOG_DIR/configs.sh
chown -R pcp:pcp /var/log/pcp/pmcd 2>/dev/null
chown -R pcp:pcp /var/log/pcp/pmlogger 2>/dev/null
chown -R pcp:pcp /var/log/pcp/sa 2>/dev/null
chown -R pcp:pcp /var/log/pcp/pmie 2>/dev/null
chown -R pcp:pcp /var/log/pcp/pmproxy 2>/dev/null
touch "$PCP_PMNS_DIR/.NeedRebuild"
chmod 644 "$PCP_PMNS_DIR/.NeedRebuild"


if [ $1 -ge 1 ] && [ -x /usr/bin/systemctl ] ; then
        /usr/bin/systemctl try-restart pmcd.service || :
fi


if [ $1 -eq 1 ] && [ -x /usr/bin/systemctl ] ; then
        /usr/bin/systemctl --no-reload preset pmcd.service || :
fi


if [ $1 -ge 1 ] && [ -x /usr/bin/systemctl ] ; then
        /usr/bin/systemctl try-restart pmlogger.service || :
fi


if [ $1 -eq 1 ] && [ -x /usr/bin/systemctl ] ; then
        /usr/bin/systemctl --no-reload preset pmlogger.service || :
fi


if [ $1 -ge 1 ] && [ -x /usr/bin/systemctl ] ; then
        /usr/bin/systemctl try-restart pmie.service || :
fi


if [ $1 -eq 1 ] && [ -x /usr/bin/systemctl ] ; then
        /usr/bin/systemctl --no-reload preset pmie.service || :
fi
    systemctl condrestart pmproxy.service >/dev/null 2>&1
cd $PCP_PMNS_DIR && ./Rebuild -s && rm -f .NeedRebuild
cd
/sbin/ldconfig
%postun
/sbin/ldconfig


%preun selinux
if [ "$1" -ge 1 ]
then
    /usr/libexec/pcp/bin/selinux-setup /var/lib/pcp/selinux install "pcpupstream"
elif [ "$1" -eq 0 ]
then
    /usr/libexec/pcp/bin/selinux-setup /var/lib/pcp/selinux remove "pcpupstream"
fi
%triggerun selinux -- docker-selinux
if [ "$1" -ge 1 ]
then
    /usr/libexec/pcp/bin/selinux-setup /var/lib/pcp/selinux install "pcpupstream-docker"
elif [ "$1" -eq 0 ]
then
    /usr/libexec/pcp/bin/selinux-setup /var/lib/pcp/selinux remove "pcpupstream-docker"
fi
%triggerun selinux -- container-selinux
if [ "$1" -ge 1 ]
then
    /usr/libexec/pcp/bin/selinux-setup /var/lib/pcp/selinux install "pcpupstream-container"
elif [ "$1" -eq 0 ]
then
    /usr/libexec/pcp/bin/selinux-setup /var/lib/pcp/selinux remove "pcpupstream-container"
fi

%files -f base.list
%doc CHANGELOG INSTALL.md README.md VERSION.pcp pcp.lsm COPYING
%dir /etc/pcp
%dir /var/lib/pcp/pmdas
%dir /usr/share/pcp
%dir /var/lib/pcp
%dir /var/lib/pcp/config
%dir %attr(0775,pcp,pcp) /var/lib/pcp/tmp
%dir %attr(0775,pcp,pcp) /var/lib/pcp/tmp/pmie
%dir %attr(0775,pcp,pcp) /var/lib/pcp/tmp/pmlogger
%dir %attr(0700,root,root) /var/lib/pcp/tmp/pmcd
%dir /usr/share/pcp/lib
/usr/share/pcp/lib/ReplacePmnsSubtree
/usr/share/pcp/lib/bashproc.sh
/usr/share/pcp/lib/lockpmns
/usr/share/pcp/lib/pmdaproc.sh
/usr/share/pcp/lib/utilproc.sh
/usr/share/pcp/lib/rc-proc.sh
/usr/share/pcp/lib/rc-proc.sh.minimal
/usr/share/pcp/lib/unlockpmns
%dir %attr(0775,pcp,pcp) /var/log/pcp
%attr(0775,pcp,pcp) /var/log/pcp/pmcd
%attr(0775,pcp,pcp) /var/log/pcp/pmlogger
%attr(0775,pcp,pcp) /var/log/pcp/pmie
%attr(0775,pcp,pcp) /var/log/pcp/pmproxy
/var/lib/pcp/pmns
/usr/share/pcp/lib/pcp
/usr/share/pcp/lib/pmcd
/usr/share/pcp/lib/pmlogger
/usr/share/pcp/lib/pmie
/usr/share/pcp/lib/pmproxy
/usr/lib/systemd/system/pmcd.service
/usr/lib/systemd/system/pmlogger.service
/usr/lib/systemd/system/pmie.service
/usr/lib/systemd/system/pmproxy.service
%config(noreplace) /etc/sasl2/pmcd.conf
%config(noreplace) /etc/cron.d/pcp-pmlogger
%config(noreplace) /etc/cron.d/pcp-pmie
%config(noreplace) /etc/sysconfig/pmlogger
%config(noreplace) /etc/sysconfig/pmproxy
%config(noreplace) /etc/sysconfig/pmcd
%config /etc/pcp.env
%dir /etc/pcp/labels
%dir /etc/pcp/pmcd
%config(noreplace) /etc/pcp/pmcd/pmcd.conf
%config(noreplace) /etc/pcp/pmcd/pmcd.options
%config(noreplace) /etc/pcp/pmcd/rc.local
%dir /etc/pcp/pmproxy
%config(noreplace) /etc/pcp/pmproxy/pmproxy.options
%dir /etc/pcp/pmie
%dir /etc/pcp/pmie/control.d
%config(noreplace) /etc/pcp/pmie/control
%config(noreplace) /etc/pcp/pmie/control.d/local
%dir /etc/pcp/pmlogger
%dir /etc/pcp/pmlogger/control.d
%config(noreplace) /etc/pcp/pmlogger/control
%config(noreplace) /etc/pcp/pmlogger/control.d/local
%dir %attr(0775,pcp,pcp) /etc/pcp/nssdb
%dir /etc/pcp/discover
%config(noreplace) /etc/pcp/discover/pcp-kube-pods.conf
%ghost %dir %attr(0775,pcp,pcp) /var/run/pcp
/var/lib/pcp/config/pmafm
%dir %attr(0775,pcp,pcp) /var/lib/pcp/config/pmie
/var/lib/pcp/config/pmie
/var/lib/pcp/config/pmieconf
%dir %attr(0775,pcp,pcp) /var/lib/pcp/config/pmlogger
/var/lib/pcp/config/pmlogger/*
/var/lib/pcp/config/pmlogrewrite
%dir %attr(0775,pcp,pcp) /var/lib/pcp/config/pmda
/usr/share/bash-completion/completions/*
/usr/share/zsh/site-functions/_pcp
/usr/share/systemtap/tapset/pmcd.stp
/usr/lib64/libpcp.so.3
/usr/lib64/libpcp_gui.so.2
/usr/lib64/libpcp_mmv.so.1
/usr/lib64/libpcp_pmda.so.3
/usr/lib64/libpcp_trace.so.2
/usr/lib64/libpcp_import.so.1
/usr/lib64/libpcp_web.so.1
%exclude /var/lib/pcp/pmns/.NeedRebuild
%exclude /usr/libexec/pcp/bin/pcp-ipcs
%exclude /usr/libexec/pcp/bin/pcp-lvmcache

%files monitor

%files collector

%files zeroconf
/usr/libexec/pcp/bin/pmlogger_daily_report
%config(noreplace) /etc/cron.d/pcp-pmlogger-daily-report
/var/lib/pcp/config/pmlogconf/zeroconf

%files conf
%dir /usr/include/pcp
/usr/include/pcp/builddefs
/usr/include/pcp/buildrules
%config /etc/pcp.conf
%dir /var/lib/pcp/config/derived
%config /var/lib/pcp/config/derived/*



%files devel -f devel.list
/usr/share/pcp/examples
/var/lib/pcp/pmdas/simple
/var/lib/pcp/pmdas/sample
/var/lib/pcp/pmdas/trivial
/var/lib/pcp/pmdas/txmon
/usr/lib64/libpcp.so
/usr/lib64/libpcp_gui.so
/usr/lib64/libpcp_mmv.so
/usr/lib64/libpcp_pmda.so
/usr/lib64/libpcp_trace.so
/usr/lib64/libpcp_import.so
/usr/lib64/libpcp_web.so
/usr/lib64/pkgconfig/libpcp.pc
/usr/lib64/pkgconfig/libpcp_pmda.pc
/usr/lib64/pkgconfig/libpcp_import.pc
/usr/include/pcp/*.h
%defattr(-,pcpqa,pcpqa)
/var/lib/pcp/testsuite

%files webapi
/usr/share/pcp/lib/pmwebd
/usr/lib/systemd/system/pmwebd.service
/usr/libexec/pcp/bin/pmwebd
%attr(0775,pcp,pcp) /var/log/pcp/pmwebd
/etc/pcp/pmwebd
%config(noreplace) /etc/pcp/pmwebd/pmwebd.options
%dir /usr/share/pcp
%dir /usr/share/pcp/webapps

%files webjs
%dir /usr/share/pcp
%dir /usr/share/pcp/webapps
/usr/share/pcp/webapps/*.png
/usr/share/pcp/webapps/*.ico
/usr/share/pcp/webapps/*.html

%files webapp-blinkenlights
%dir /usr/share/pcp
%dir /usr/share/pcp/webapps
/usr/share/pcp/webapps/blinkenlights

%files webapp-grafana
%dir /usr/share/pcp
%dir /usr/share/pcp/webapps
/usr/share/pcp/webapps/grafana

%files webapp-graphite
%dir /usr/share/pcp
%dir /usr/share/pcp/webapps
/usr/share/pcp/webapps/graphite

%files webapp-vector
%dir /usr/share/pcp
%dir /usr/share/pcp/webapps
/usr/share/pcp/webapps/vector

%files manager
/usr/share/pcp/lib/pmmgr
/usr/lib/systemd/system/pmmgr.service
/usr/libexec/pcp/bin/pmmgr
%attr(0775,pcp,pcp) /var/log/pcp/pmmgr
%config(missingok,noreplace) /etc/pcp/pmmgr
%config(noreplace) /etc/pcp/pmmgr/pmmgr.options

%files import-sar2pcp
/usr/bin/sar2pcp

%files import-iostat2pcp
/usr/bin/iostat2pcp

%files import-mrtg2pcp
/usr/bin/mrtg2pcp

%files import-ganglia2pcp
/usr/bin/ganglia2pcp

%files import-collectl2pcp
/usr/bin/collectl2pcp

%files pmda-papi
/var/lib/pcp/pmdas/papi

%files pmda-perfevent
/var/lib/pcp/pmdas/perfevent
%config(noreplace) /var/lib/pcp/pmdas/perfevent/perfevent.conf

%files pmda-infiniband
/var/lib/pcp/pmdas/ib
/var/lib/pcp/pmdas/infiniband

%files pmda-activemq
/var/lib/pcp/pmdas/activemq

%files pmda-bonding
/var/lib/pcp/pmdas/bonding

%files pmda-bind2
/var/lib/pcp/pmdas/bind2

%files pmda-dbping
/var/lib/pcp/pmdas/dbping

%files pmda-ds389log
/var/lib/pcp/pmdas/ds389log

%files pmda-ds389
/var/lib/pcp/pmdas/ds389

%files pmda-elasticsearch
/var/lib/pcp/pmdas/elasticsearch

%files pmda-gpfs
/var/lib/pcp/pmdas/gpfs

%files pmda-gpsd
/var/lib/pcp/pmdas/gpsd

%files pmda-docker
/var/lib/pcp/pmdas/docker

%files pmda-lio
/var/lib/pcp/pmdas/lio

%files pmda-prometheus
/var/lib/pcp/pmdas/prometheus

%files pmda-lustre
/var/lib/pcp/pmdas/lustre

%files pmda-lustrecomm
/var/lib/pcp/pmdas/lustrecomm

%files pmda-memcache
/var/lib/pcp/pmdas/memcache

%files pmda-mysql
/var/lib/pcp/pmdas/mysql

%files pmda-named
/var/lib/pcp/pmdas/named

%files pmda-netfilter
/var/lib/pcp/pmdas/netfilter

%files pmda-news
/var/lib/pcp/pmdas/news

%files pmda-nginx
/var/lib/pcp/pmdas/nginx

%files pmda-nfsclient
/var/lib/pcp/pmdas/nfsclient

%files pmda-oracle
/var/lib/pcp/pmdas/oracle

%files pmda-pdns
/var/lib/pcp/pmdas/pdns

%files pmda-postfix
/var/lib/pcp/pmdas/postfix

%files pmda-postgresql
/var/lib/pcp/pmdas/postgresql

%files pmda-redis
/var/lib/pcp/pmdas/redis

%files pmda-rsyslog
/var/lib/pcp/pmdas/rsyslog

%files pmda-samba
/var/lib/pcp/pmdas/samba

%files pmda-snmp
/var/lib/pcp/pmdas/snmp

%files pmda-slurm
/var/lib/pcp/pmdas/slurm

%files pmda-vmware
/var/lib/pcp/pmdas/vmware

%files pmda-zimbra
/var/lib/pcp/pmdas/zimbra

%files pmda-dm
/var/lib/pcp/pmdas/dm

%if !%{disable_bcc}
%files pmda-bcc
/var/lib/pcp/pmdas/bcc
%endif

%files pmda-gluster
/var/lib/pcp/pmdas/gluster

%files pmda-zswap
/var/lib/pcp/pmdas/zswap

%files pmda-unbound
/var/lib/pcp/pmdas/unbound

%files pmda-mic
/var/lib/pcp/pmdas/mic

%files pmda-haproxy
/var/lib/pcp/pmdas/haproxy

%files pmda-libvirt
/var/lib/pcp/pmdas/libvirt

%files export-pcp2graphite
/usr/bin/pcp2graphite

%files export-pcp2influxdb
/usr/bin/pcp2influxdb

%files export-pcp2json
/usr/bin/pcp2json

%files export-pcp2spark
/usr/bin/pcp2spark


%files export-pcp2xml
/usr/bin/pcp2xml

%files export-pcp2zabbix
/usr/bin/pcp2zabbix

%files export-zabbix-agent
/usr/lib64/zabbix
/etc/zabbix/zabbix_agentd.d/zbxpcp.conf

%files pmda-json
/var/lib/pcp/pmdas/json

%files pmda-apache
/var/lib/pcp/pmdas/apache

%files pmda-bash
/var/lib/pcp/pmdas/bash

%files pmda-cifs
/var/lib/pcp/pmdas/cifs

%files pmda-cisco
/var/lib/pcp/pmdas/cisco

%files pmda-gfs2
/var/lib/pcp/pmdas/gfs2

%files pmda-lmsensors
/var/lib/pcp/pmdas/lmsensors

%files pmda-logger
/var/lib/pcp/pmdas/logger

%files pmda-mailq
/var/lib/pcp/pmdas/mailq

%files pmda-mounts
/var/lib/pcp/pmdas/mounts

%files pmda-nvidia-gpu
/var/lib/pcp/pmdas/nvidia

%files pmda-roomtemp
/var/lib/pcp/pmdas/roomtemp

%files pmda-rpm
/var/lib/pcp/pmdas/rpm

%files pmda-sendmail
/var/lib/pcp/pmdas/sendmail

%files pmda-shping
/var/lib/pcp/pmdas/shping

%files pmda-smart
/var/lib/pcp/pmdas/smart

%files pmda-summary
/var/lib/pcp/pmdas/summary

%files pmda-systemd
/var/lib/pcp/pmdas/systemd

%files pmda-trace
/var/lib/pcp/pmdas/trace

%files pmda-weblog
/var/lib/pcp/pmdas/weblog

%files -n perl-PCP-PMDA -f perl-pcp-pmda.list

%files -n perl-PCP-MMV -f perl-pcp-mmv.list

%files -n perl-PCP-LogImport -f perl-pcp-logimport.list

%files -n perl-PCP-LogSummary -f perl-pcp-logsummary.list

%files -n python3-pcp -f python3-pcp.list.rpm

%files gui -f pcp-gui.list
/etc/pcp/pmsnap
%config(noreplace) /etc/pcp/pmsnap/control
/var/lib/pcp/config/pmsnap
/var/lib/pcp/config/pmchart
/var/lib/pcp/config/pmafm/pcp-gui
/usr/share/applications/pmchart.desktop

%files help -f pcp-doc.list

%files selinux -f pcp-selinux.list

%files system-tools -f pcp-system-tools.list
%dir /etc/pcp/dstat
%dir /etc/pcp/pmrep
%config(noreplace) /etc/pcp/dstat/*
%config(noreplace) /etc/pcp/pmrep/*

%changelog
* Wed Oct 21 2020 wangxiao <wangxiao65@huawei.com> - 4.1.3-14
- drop python2 subpackage

* Tue Sep 22 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 4.1.3-13
- fix pcp pidstat bug  -a option

* Fri Sep 18 2020 Guoshuai Sun <sunguoshuai@huawei.com> - 4.1.3-12
- Fix some bugs in pcp-pidstat,include -U option error,-? need an arg,
  And del unused -h/--host options

* Thu Sep 17 2020 lingsheng <lingsheng@huawei.com> - 4.1.3-11
- Fix collect2pcp option

* Thu Sep 16 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 4.1.3-10
- Fix some options and syntax errors

* Fri Aug 28 2020 lingsheng <lingsheng@huawei.com> - 4.1.3-9
- Fix .NeedRebuild unfound when removing rpm

* Tue Aug 04 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 4.1.3-8
- Fix compile failure in make multithreading

* Tue Jul 28 2020 lingsheng <lingsheng@huawei.com> - 4.1.3-7
- change require to rdma-core as libibmad and libibumad obsoleted

* Wed Jul 15 2020 huanghaitao <huanghaitao@huawei.com> - 4.1.3-6
- change undelete rebuild flag file .NeedRebuild

* Tue Jun 23 2020 panchenbo <panchenbo@uniontech.com> - 4.1.3-5
- Type:bugfix
- ID: NA
- SUG: install
- DESC: Fix pcp-pmda-activemq install error

* Wed May 13 2020 huanghaitao <huanghaitao8@huawei.com> - 4.1.3-4
- Type:cves
- ID: CVE-2019-3695 CVE-2019-3696 
- SUG:restart
- DESC: fix CVE-2019-3695 CVE-2019-3696

* Fri Feb 21 2020 Senlin Xia <xiasenlin1@huawei.com> - 4.1.3-3
- package init
